function [assocProbExisting,assocProbNew,messagelhfRatios] = dataAssociationBP( hypExisting, hypNew, threshold, numIterations )
[m,n] = size(hypExisting);
m = m-1;
assocProbNew = ones(m,1);
assocProbExisting = ones(m+1,n);
messagelhfRatios = ones(m,n);

if(n==0 || m==0) 
    return;
end

if(isempty(hypNew))
    hypNew = 1;
end
w = hypExisting(2:end,:);
w_i0 = hypExisting(1,:);
w_j0 = hypNew;

om = ones(1,m);
on = ones(1,n);
muba = ones(m,n);

for iteration = 1:numIterations
    mubaOld = muba;
    
    prodfact = muba .* w;
    sumprod = w_i0 + sum(prodfact);
    muab = w ./ (sumprod(om,:) - prodfact);
    summuab = w_j0 + sum(muab,2);
    muba = 1 ./ (summuab(:,on) - muab);
    
    improvement = sqrt(mean(mean((muba-mubaOld).^2)));
    if(improvement < threshold)
        break
    end
end

assocProbExisting(1,:) = w_i0;
assocProbExisting(2:end,:) = hypExisting(2:end,:).*muba;

for target=1:n
    assocProbExisting(:,target) = assocProbExisting(:,target)/sum(assocProbExisting(:,target));
end

messagelhfRatios = muba;
assocProbNew = w_j0./summuab;
end